package com.nicklase.test1;

import com.nicklase.test1.sensors.BarometricSensor;
import com.nicklase.test1.sensors.BatterySensor;
import com.nicklase.test1.sensors.HeartRateSensor;
import com.nicklase.test1.sensors.LocationSensor;
import com.nicklase.test1.sensors.PowerSensor;
import com.nicklase.test1.sensors.SpeedSensor;
import com.nicklase.test1.sensors.TimeSensor;

import android.app.Activity;

import android.os.SystemClock;

public class Nicklas extends Thread
{
  public final static int SENSOR_ALL = 0;
  public final static int SENSOR_HR = 1;
  public final static int SENSOR_TIME = 2;
  public final static int SENSOR_BATT = 3;
  public final static int SENSOR_BARO = 4;
  public final static int SENSOR_LOC = 5;
  public final static int SENSOR_SPEED = 6;
  public final static int SENSOR_POW = 7;

  private final static String TAG = "Nicklas";
  private final static Class<?> sSensors[] = { HeartRateSensor.class,
      TimeSensor.class, BatterySensor.class, BarometricSensor.class,
      LocationSensor.class, SpeedSensor.class, PowerSensor.class };

  private static final long MAXTIME = 1000 * 60 * 5;
  private static Nicklas instance = null;
  private Activity mActivity = null;
  private volatile boolean mCont = true;
  private MySensor mSensors[] = null;
  private volatile boolean mReset = false;

  public static synchronized void init(Activity a)
  {
    if (instance == null)
    {
      instance = new Nicklas();
      instance.mActivity = a;
      instance.start();
    }
  }

  public void run()
  {
    MyLog.log(TAG, "Nicklas loop...");
    initSensors();
    long tlast = System.currentTimeMillis();
    while (mCont)
    {
      SystemClock.sleep(1000);
      if (mReset)
      {
        MyLog.log(TAG, "reset...");
        deinitSensors();
        System.gc();
        SystemClock.sleep(5000);
        mReset = false;
        initSensors();
        MyLog.log(TAG, "reset complete.");
      }
      long t = System.currentTimeMillis();
      if (checkSensors())
      {
        tlast = t;
      }
      else
      {
        MyLog.log(TAG, "Sensors inactive: " + (t - tlast));
        if ((t - tlast) > MAXTIME)
        {
          mCont = false;
        }
      }
    }
    deinitSensors();
    MyLog.log(TAG, "Exiting.");
    SystemClock.sleep(10000);
    System.exit(0);
  }

  private boolean checkSensors()
  {
    for (MySensor tmp : mSensors)
    {
      if (tmp.getStatus().mState == StatusValue.RUNNING)
        return true;
    }
    return false;
  }

  private void deinitSensors()
  {
    MyLog.log(TAG, "deinitSensors...");
    for (int i = 0; i < mSensors.length; i++)
    {
      mSensors[i].deinit();
      mSensors[i] = null;
    }
    mSensors = null;
    MyLog.log(TAG, "deinitSensors complete.");
  }

  private void initSensors()
  {
    MyLog.log(TAG, "initSensors...");
    if (mSensors != null)
      deinitSensors();
    mSensors = new MySensor[sSensors.length];
    for (int i = 0; i < mSensors.length; i++)
    {
      try
      {
        mSensors[i] = (MySensor) sSensors[i].newInstance();
        MyLog.log(TAG, "Init sensor: " + mSensors[i].getName());
        mSensors[i].init(mActivity);
        while (!mReset && mSensors[i].getStatus().mState != StatusValue.RUNNING
            && mSensors[i].getStatus().mState != StatusValue.SHUTDOWN)
        {
          SystemClock.sleep(200);
        }
        MyLog.log(TAG, "Complete init sensor: " + mSensors[i].getName()
            + " status=" + mSensors[i].getStatus().mState);
      }
      catch (Throwable t)
      {
        MyLog.log(TAG, "Incorrect sensor class: " + t.toString());
      }
    }
    MyLog.log(TAG, "initSensors complete.");
  }

  public static String getStatus(int sensor, int index)
  {
    if (instance == null)
    {
      return "Initilizing...";
    }
    else
    {
      return instance.getStatusImpl(sensor, index);
    }
  }

  public String getStatusImpl(int sensor, int index)
  {
    if (mSensors == null)
      return "Nicklas\nEkstrand";
    StringBuffer sb = new StringBuffer();
    if (sensor == SENSOR_ALL)
    {
      for (int i = 0; i < mSensors.length; i++)
      {
        try
        {
          sb.append(mSensors[i].getStatus().mString);
        }
        catch (Throwable t)
        {
          sb.append("?");
        }
        sb.append(" ");
      }
    }
    else
    {
      try
      {
        sb.append(mSensors[sensor - 1].getStatusRaw(index));
      }
      catch (Throwable t)
      {
        sb.append("?");
      }
    }
    return sb.toString();
  }

  public static void reset()
  {
    instance.mReset = true;
  }
}
