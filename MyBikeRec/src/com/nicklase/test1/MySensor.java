package com.nicklase.test1;

import android.app.Activity;

public interface MySensor
{
  public void init(Activity a);

  public void deinit();

  public StatusValue getStatus();

  public String getStatusRaw(int index);

  public String getName();
}
