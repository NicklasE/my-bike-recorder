package com.nicklase.test1;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;

public class MyLog extends Thread {
	private final static String TAG = "MyLog";
	private static MyLog instance = null;

	static {
		instance = new MyLog();
		instance.start();
	}

	private ArrayList<String> mMessages = new ArrayList<String>();
	private Object mLock = new Object();
	private File mFile;
	private FileOutputStream mFos;

	private MyLog() {
		File f1 = Environment.getExternalStorageDirectory();
		File f2 = new File(f1.getAbsolutePath() + "/nicklas");
		f2.mkdirs();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.ENGLISH);
		String name = f2.getAbsolutePath() + "/data" + sdf.format(new Date())
				+ ".txt";
		mFile = new File(name);
		try {
			mFos = new FileOutputStream(mFile);
		} catch (Throwable t) {
			log(TAG, "Error while init log file: " + t.toString());
		}
	}

	public static void log(String tag, String str) {
		synchronized (instance.mLock) {
			instance.mMessages.add(System.currentTimeMillis() + ";" + tag + ";"
					+ str + "\n");
		}
	}

	public void run() {
		log(TAG, "MyLog thread started");
		while (true) {
			SystemClock.sleep(10000);
			ArrayList<String> tmp = null;
			synchronized (mLock) {
				if (mMessages.size() > 0) {
					tmp = mMessages;
					mMessages = new ArrayList<String>();
				}
			}
			if (tmp != null) {
				write2file(tmp);
			}
		}
	}

	private void write2file(ArrayList<String> tmp) {
		try {
			for (String str : tmp) {
				Log.i(TAG, str);
				mFos.write((str).getBytes());
			}
			mFos.flush();
		} catch (Throwable t) {
			log(TAG, "Fail write2file: " + t.toString());
		}
	}
}
