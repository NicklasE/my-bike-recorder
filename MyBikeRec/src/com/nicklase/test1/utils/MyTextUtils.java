package com.nicklase.test1.utils;

import java.text.NumberFormat;

public class MyTextUtils 
{
	public static NumberFormat sNF2 = NumberFormat.getNumberInstance();
	public static NumberFormat sNF1digit = NumberFormat.getNumberInstance();
	static
	{
		sNF2.setMinimumIntegerDigits(2);
		sNF1digit.setMaximumFractionDigits(1);
		sNF1digit.setGroupingUsed(false);
	}
}
