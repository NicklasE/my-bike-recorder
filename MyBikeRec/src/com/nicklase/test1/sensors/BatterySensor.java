package com.nicklase.test1.sensors;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import com.nicklase.test1.MyLog;
import com.nicklase.test1.MySensor;
import com.nicklase.test1.StatusValue;
import com.nicklase.test1.utils.MyTextUtils;

public class BatterySensor implements MySensor
{
  private final static String TAG = "Battery";
  private StatusValue mStatus = new StatusValue();
  private int mTemp = 0;
  private int mLevel = 0;
  private Activity mActivity;

  public BatterySensor()
  {
    mStatus.mString = "";
  }

  public void init(Activity a)
  {
    mStatus.mState = StatusValue.SHUTDOWN;
    mActivity = a;
    mActivity.registerReceiver(mBattRec, new IntentFilter(
        Intent.ACTION_BATTERY_CHANGED));
  }

  public void deinit()
  {
    mStatus.mState = StatusValue.SHUTDOWN;
  }

  public StatusValue getStatus()
  {
    double temp = (double) mTemp / 10.0;
    mStatus.mString = "Batt=" + mLevel + " Btemp="
        + MyTextUtils.sNF1digit.format(temp);
    return mStatus;
  }
  
  public String getStatusRaw (int index)
  {
    if (index == 0)
    {
      return "" + mLevel;
    }
    return "" + mTemp;
  }

  private BroadcastReceiver mBattRec = new BroadcastReceiver()
  {
    public void onReceive(Context context, Intent intent)
    {
      int health = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, 0);
      int icon_small = intent.getIntExtra(BatteryManager.EXTRA_ICON_SMALL, 0);
      int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
      int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0);
      boolean present = intent.getExtras().getBoolean(
          BatteryManager.EXTRA_PRESENT);
      int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 0);
      int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, 0);
      String technology = intent.getExtras().getString(
          BatteryManager.EXTRA_TECHNOLOGY);
      int temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0);
      int voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0);

      mLevel = level;
      mTemp = temperature;
      MyLog.log(TAG, "health=" + health);
      MyLog.log(TAG, "icon=" + icon_small);
      MyLog.log(TAG, "level=" + level);
      MyLog.log(TAG, "plugged=" + plugged);
      MyLog.log(TAG, "present=" + present);
      MyLog.log(TAG, "scale=" + scale);
      MyLog.log(TAG, "status=" + status);
      MyLog.log(TAG, "tech=" + technology);
      MyLog.log(TAG, "temp=" + temperature);
      MyLog.log(TAG, "volt=" + voltage);
    }
  };

  public String getName()
  {
    return TAG;
  }
}
