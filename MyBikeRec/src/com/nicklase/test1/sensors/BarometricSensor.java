package com.nicklase.test1.sensors;

import android.app.Activity;
import android.app.Service;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.nicklase.test1.MyLog;
import com.nicklase.test1.MySensor;
import com.nicklase.test1.StatusValue;
import com.nicklase.test1.utils.MyTextUtils;

public class BarometricSensor implements MySensor, SensorEventListener
{
  private static final String TAG = "Barometric";
  private Activity mActivity;
  private StatusValue mStatus = new StatusValue();
  private String mRaw = "n/a";
  
  public void init(Activity a)
  {
    mStatus.mState = StatusValue.SHUTDOWN;
    mActivity = a;
    SensorManager sensorManager = (SensorManager) mActivity
        .getSystemService(Service.SENSOR_SERVICE);
    Sensor pressureSensor = sensorManager
        .getDefaultSensor(Sensor.TYPE_PRESSURE);
    sensorManager.registerListener(this, pressureSensor,
        SensorManager.SENSOR_DELAY_NORMAL);
  }

  public void deinit()
  {
    mStatus.mState = StatusValue.SHUTDOWN;
    SensorManager sensorManager = (SensorManager) mActivity
        .getSystemService(Service.SENSOR_SERVICE);
    sensorManager.unregisterListener(this);
  }

  public StatusValue getStatus()
  {
    return mStatus;
  }

  public String getStatusRaw(int index)
  {
    return mRaw;
  }
  
  public void onAccuracyChanged(Sensor arg0, int arg1)
  {
    MyLog.log(TAG, "accuracyChange: " + arg0.toString() + "=" + arg1);
  }

  @Override
  public void onSensorChanged(SensorEvent arg0)
  {
    try
    {
      float values[] = arg0.values;
      StringBuffer sb = new StringBuffer();
      if (values != null)
      {
        for (int i = 0; i < values.length; i++)
        {
          sb.append(values[i]);
          sb.append(";");
        }
        sb.append(values.length);
        mRaw = MyTextUtils.sNF1digit.format(values[0]);
        mStatus.mString = "Press=" + mRaw;
      }
      String str = sb.toString();
      MyLog.log(TAG, str);
    }
    catch (Throwable t)
    {
      MyLog.log(TAG, "onSensorChanged: " + t.toString());
      mRaw = "n/a";
    }
  }

  public String getName()
  {
    return TAG;
  }
}
