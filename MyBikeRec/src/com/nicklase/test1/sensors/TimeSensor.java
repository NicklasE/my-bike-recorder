package com.nicklase.test1.sensors;

import android.app.Activity;

import com.nicklase.test1.MySensor;
import com.nicklase.test1.StatusValue;
import com.nicklase.test1.utils.MyTextUtils;

public class TimeSensor implements MySensor
{
  private final static String TAG = "Time";
  private long t0 = System.currentTimeMillis();
  private StatusValue mStatus = new StatusValue();
  private String mRaw = "n/a";

  public void init(Activity a)
  {
    mStatus.mState = StatusValue.SHUTDOWN;
  }

  public void deinit()
  {
    mStatus.mState = StatusValue.SHUTDOWN;
  }

  public StatusValue getStatus()
  {
    long t = System.currentTimeMillis() - t0;
    long s = t / 1000;
    long m = s / 60;
    long h = m / 60;
    m = m - h * 60;
    s = s - 60 * m - 3600 * h;
    mRaw = MyTextUtils.sNF2.format(h) + ":" + MyTextUtils.sNF2.format(m) + ":"
        + MyTextUtils.sNF2.format(s);
    mStatus.mString = "T=" + mRaw;
    return mStatus;
  }

  public String getStatusRaw(int index)
  {
    getStatus();
    return mRaw;
  }

  public String getName()
  {
    return TAG;
  }

}
