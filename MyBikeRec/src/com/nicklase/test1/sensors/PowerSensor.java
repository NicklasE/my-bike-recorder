package com.nicklase.test1.sensors;

import java.math.BigDecimal;
import java.util.EnumSet;

import android.app.Activity;

import com.dsi.ant.plugins.antplus.pcc.AntPlusBikePowerPcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikePowerPcc.CalculatedWheelDistanceReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikePowerPcc.ICalculatedCrankCadenceReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikePowerPcc.ICalculatedPowerReceiver;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikePowerPcc.DataSource;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IDeviceStateChangeReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IPluginAccessResultReceiver;
import com.nicklase.test1.MyLog;
import com.nicklase.test1.MySensor;
import com.nicklase.test1.StatusValue;

public class PowerSensor implements MySensor
{
  private final static String TAG = "Power";
  private Activity mActivity = null;
  AntPlusBikePowerPcc powerPcc = null;
  private int mPower = 0;
  private StatusValue mStatus = new StatusValue();
  private int mRevs = 0;
  private int mRpm = 0;

  public PowerSensor()
  {
    mStatus.mState = StatusValue.INITIALIZING;
  }

  public void init(Activity a)
  {
    mActivity = a;
    initImpl();
  }

  public void deinit()
  {
    mStatus.mState = StatusValue.SHUTDOWN;
    if (powerPcc != null)
    {
      powerPcc.releaseAccess();
      powerPcc = null;
    }
  }

  public StatusValue getStatus()
  {
    if (mStatus.mState == StatusValue.RUNNING)
    {
      mStatus.mString = "PWR=" + mPower + " revs=" + mRevs + " rpm=" + mRpm;
    }
    else
    {
      mStatus.mString = "PWR=n/a";
    }
    return mStatus;
  }

  public String getStatusRaw(int index)
  {
    if (mStatus.mState == StatusValue.RUNNING)
    {
      return "" + mPower;
    }
    else
    {
      return "n/a";
    }
  }

  private void initImpl()
  {
    mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        AntPlusBikePowerPcc.requestAccess(mActivity,
            mActivity.getApplicationContext(),
            base_IPluginAccessResultReceiver, base_IDeviceStateChangeReceiver);
      }
    });
  }

  protected IPluginAccessResultReceiver<AntPlusBikePowerPcc> base_IPluginAccessResultReceiver = new IPluginAccessResultReceiver<AntPlusBikePowerPcc>()
  {
    // Handle the result, connecting to events on success or reporting
    // failure to user.
    public void onResultReceived(AntPlusBikePowerPcc result,
        RequestAccessResult resultCode, DeviceState initialDeviceState)
    {
      // showDataDisplay("Connecting...");
      MyLog.log(TAG, "connecting...");
      switch (resultCode)
      {
      case SUCCESS:
        powerPcc = result;
        // tv_status.setText(result.getDeviceName() + ": "
        // + initialDeviceState);
        subscribeToPowerEvents();
        mStatus.mState = StatusValue.RUNNING;
        break;
      case CHANNEL_NOT_AVAILABLE:
        MyLog.log(TAG, "Error. Do Menu->Reset.");
        mStatus.mState = StatusValue.SHUTDOWN;
        break;
      case OTHER_FAILURE:
        MyLog.log(TAG, "Error. Do Menu->Reset.");
        break;
      case DEPENDENCY_NOT_INSTALLED:
        MyLog.log(TAG, "Dependency missing");
        mStatus.mState = StatusValue.SHUTDOWN;
        break;
      case USER_CANCELLED:
        MyLog.log(TAG, "Cancelled. Do Menu->Reset.");
        mStatus.mState = StatusValue.SHUTDOWN;
        break;
      case UNRECOGNIZED:
        // TODO This flag indicates that an unrecognized value was sent
        // by the service, an upgrade of your PCC may be required to
        // handle this new value.
        MyLog.log(TAG, "Error. Do Menu->Reset.");
        mStatus.mState = StatusValue.SHUTDOWN;
        break;
      default:
        MyLog.log(TAG, "Error. Do Menu->Reset.");
        mStatus.mState = StatusValue.SHUTDOWN;
        break;
      }
    }
  };

  // Receives state changes and shows it on the status display line
  protected IDeviceStateChangeReceiver base_IDeviceStateChangeReceiver = new IDeviceStateChangeReceiver()
  {
    public void onDeviceStateChange(final DeviceState newDeviceState)
    {
      mActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          AntPlusBikePowerPcc tmp = powerPcc;
          if (tmp != null)
          {
            MyLog.log(TAG, tmp.getDeviceName() + ": " + newDeviceState);
          }
          if (newDeviceState == DeviceState.DEAD)
            powerPcc = null;
        }
      });

    }
  };

  public void subscribeToPowerEvents()
  {
    powerPcc.subscribeCalculatedPowerEvent(new ICalculatedPowerReceiver()
    {

      public void onNewCalculatedPower(final long estTimestamp,
          final EnumSet<EventFlag> eventFlags, final DataSource dataSource,
          final BigDecimal calculatedPower)
      {
        mPower = calculatedPower.intValue();
        MyLog.log(TAG, "calcpwr=" + mPower);
      }
    });

    powerPcc
        .subscribeCalculatedWheelDistanceEvent(new CalculatedWheelDistanceReceiver(
            new BigDecimal(1))
        {
          public void onNewCalculatedWheelDistance(final long estTimestamp,
              final EnumSet<EventFlag> eventFlags, final DataSource dataSource,
              final BigDecimal calculatedWheelDistance)
          {
            mRevs = calculatedWheelDistance.intValue();
            MyLog.log(TAG, "revs=" + mRevs);
          }
        });

    powerPcc
        .subscribeCalculatedCrankCadenceEvent(new ICalculatedCrankCadenceReceiver()
        {
          public void onNewCalculatedCrankCadence(final long estTimestamp,
              final EnumSet<EventFlag> eventFlags, final DataSource dataSource,
              final BigDecimal calculatedCrankCadence)
          {
            mRpm = calculatedCrankCadence.intValue();
            MyLog.log(TAG, "rpm=" + mRpm);
          }
        });

  }

  public String getName()
  {
    return TAG;
  }

}
