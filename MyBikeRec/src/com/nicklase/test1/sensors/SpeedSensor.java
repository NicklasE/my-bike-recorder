package com.nicklase.test1.sensors;

import java.math.BigDecimal;
import java.util.EnumSet;

import android.app.Activity;

import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc.IRawSpeedAndDistanceDataReceiver;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IDeviceStateChangeReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IPluginAccessResultReceiver;
import com.nicklase.test1.MyLog;
import com.nicklase.test1.MySensor;
import com.nicklase.test1.StatusValue;

public class SpeedSensor implements MySensor
{
  private final static String TAG = "Speed";
  private Activity mActivity = null;
  AntPlusBikeSpeedDistancePcc hrPcc = null;
  private long mRevs = 0;
  private StatusValue mStatus = new StatusValue();

  public SpeedSensor()
  {
    mStatus.mState = StatusValue.INITIALIZING;
  }

  public void init(Activity a)
  {
    mActivity = a;
    initImpl();
  }

  public void deinit()
  {
    mStatus.mState = StatusValue.SHUTDOWN;
    if (hrPcc != null)
    {
      hrPcc.releaseAccess();
      hrPcc = null;
    }
  }

  public StatusValue getStatus()
  {
    if (mStatus.mState == StatusValue.RUNNING)
    {
      mStatus.mString = "revs=" + mRevs;
    }
    else
    {
      mStatus.mString = "revs=n/a";
    }
    return mStatus;
  }

  public String getStatusRaw(int index)
  {
    if (mStatus.mState == StatusValue.RUNNING)
    {
      return "" + mRevs;
    }
    else
    {
      return "n/a";
    }
  }

  private void initImpl()
  {
    mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        AntPlusBikeSpeedDistancePcc.requestAccess(mActivity,
            mActivity.getApplicationContext(),
            base_IPluginAccessResultReceiver, base_IDeviceStateChangeReceiver);
      }
    });
  }

  protected IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc> base_IPluginAccessResultReceiver = new IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc>()
  {
    // Handle the result, connecting to events on success or reporting
    // failure to user.
    @Override
    public void onResultReceived(AntPlusBikeSpeedDistancePcc result,
        RequestAccessResult resultCode, DeviceState initialDeviceState)
    {
      // showDataDisplay("Connecting...");
      MyLog.log(TAG, "connecting...");
      switch (resultCode)
      {
      case SUCCESS:
        hrPcc = result;
        // tv_status.setText(result.getDeviceName() + ": "
        // + initialDeviceState);
        subscribeToEvents();
        mStatus.mState = StatusValue.RUNNING;
        break;
      case CHANNEL_NOT_AVAILABLE:
        MyLog.log(TAG, "Error. Do Menu->Reset.");
        mStatus.mState = StatusValue.SHUTDOWN;
        break;
      case OTHER_FAILURE:
        MyLog.log(TAG, "Error. Do Menu->Reset.");
        break;
      case DEPENDENCY_NOT_INSTALLED:
        MyLog.log(TAG, "Dependency missing");
        mStatus.mState = StatusValue.SHUTDOWN;
        break;
      case USER_CANCELLED:
        MyLog.log(TAG, "Cancelled. Do Menu->Reset.");
        mStatus.mState = StatusValue.SHUTDOWN;
        break;
      case UNRECOGNIZED:
        // TODO This flag indicates that an unrecognized value was sent
        // by the service, an upgrade of your PCC may be required to
        // handle this new value.
        MyLog.log(TAG, "Error. Do Menu->Reset.");
        mStatus.mState = StatusValue.SHUTDOWN;
        break;
      default:
        MyLog.log(TAG, "Error. Do Menu->Reset.");
        mStatus.mState = StatusValue.SHUTDOWN;
        break;
      }
    }
  };

  // Receives state changes and shows it on the status display line
  protected IDeviceStateChangeReceiver base_IDeviceStateChangeReceiver = new IDeviceStateChangeReceiver()
  {
    @Override
    public void onDeviceStateChange(final DeviceState newDeviceState)
    {
      mActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          AntPlusBikeSpeedDistancePcc tmp = hrPcc;
          if (tmp != null)
          {
            MyLog.log(TAG, tmp.getDeviceName() + ": " + newDeviceState);
          }
          if (newDeviceState == DeviceState.DEAD)
            hrPcc = null;
        }
      });

    }
  };

  public void subscribeToEvents()
  {
    hrPcc
        .subscribeRawSpeedAndDistanceDataEvent(new IRawSpeedAndDistanceDataReceiver()
        {
          public void onNewRawSpeedAndDistanceData(final long estTimestamp,
              final EnumSet<EventFlag> eventFlags,
              final BigDecimal timestampOfLastEvent,
              final long cumulativeRevolutions)
          {
            MyLog.log(TAG, "t=" + estTimestamp + " last="
                + timestampOfLastEvent + " revs=" + cumulativeRevolutions);
            mRevs = cumulativeRevolutions;
          }
        });
  }

  public String getName()
  {
    return TAG;
  }

}
