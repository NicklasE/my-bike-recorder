package com.nicklase.test1.sensors;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.nicklase.test1.MyLog;
import com.nicklase.test1.MySensor;
import com.nicklase.test1.StatusValue;
import com.nicklase.test1.utils.MyTextUtils;

public class LocationSensor implements MySensor, LocationListener
{
  private final static String TAG = "Loc";
  private StatusValue mStatus = new StatusValue();
  private Activity mActivity;
  private Location mLastLoc = null;

  public void init(Activity a)
  {
    mStatus.mState = StatusValue.SHUTDOWN;
    mActivity = a;
    final LocationSensor list = this;
    mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        MyLog.log(TAG, "register listener");
        LocationManager locationManager = (LocationManager) mActivity
            .getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
            0, list);
      }
    });
  }

  public void deinit()
  {
    MyLog.log(TAG, "removing listener");
    mStatus.mState = StatusValue.SHUTDOWN;
    LocationManager locationManager = (LocationManager) mActivity
        .getSystemService(Context.LOCATION_SERVICE);
    locationManager.removeUpdates(this);
  }

  public StatusValue getStatus()
  {
    if (mLastLoc != null)
    {
      mStatus.mString = "GPS="
          + MyTextUtils.sNF1digit.format(mLastLoc.getAccuracy()) + " speed="
          + MyTextUtils.sNF1digit.format(mLastLoc.getSpeed()) + " alt="
          + MyTextUtils.sNF1digit.format(mLastLoc.getAltitude());
    }
    else
    {
      mStatus.mString = "GPS=n/a";
    }
    return mStatus;
  }

  public String getStatusRaw(int index)
  {
    if (mLastLoc != null)
    {
      return MyTextUtils.sNF1digit.format(3.6*mLastLoc.getSpeed());
    }
    else
    {
      return "n/a";
    }
  }

  public void onLocationChanged(Location arg0)
  {
    mLastLoc = arg0;
    float acc = arg0.getAccuracy();
    double alt = arg0.getAltitude();
    float bear = arg0.getBearing();
    double lat = arg0.getLatitude();
    double lon = arg0.getLongitude();
    float speed = arg0.getSpeed();
    MyLog.log(TAG, "acc=" + acc + ";alt=" + alt + ";bear=" + bear + ";lat="
        + lat + ";lon=" + lon + ";speed=" + speed);
  }

  @Override
  public void onProviderDisabled(String arg0)
  {
    MyLog.log(TAG, "disabled=" + arg0);
  }

  @Override
  public void onProviderEnabled(String arg0)
  {
    MyLog.log(TAG, "enabled=" + arg0);
  }

  @Override
  public void onStatusChanged(String arg0, int arg1, Bundle arg2)
  {
    MyLog.log(TAG, "statuschanged=" + arg0 + ";" + arg1);
  }

  public String getName()
  {
    return TAG;
  }

}
