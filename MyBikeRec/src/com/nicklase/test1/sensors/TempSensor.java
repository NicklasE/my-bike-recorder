package com.nicklase.test1.sensors;

import android.app.Activity;
import android.app.Service;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.nicklase.test1.MyLog;
import com.nicklase.test1.MySensor;
import com.nicklase.test1.StatusValue;
import com.nicklase.test1.utils.MyTextUtils;

public class TempSensor implements MySensor, SensorEventListener
{
  private static final String TAG = "Temp";
  private Activity mActivity;
  private StatusValue mStatus = new StatusValue();
  private float mTemp = 0;
  
  public void init(Activity a)
  {
    try
    {
      mStatus.mState = StatusValue.SHUTDOWN;
      mActivity = a;
      SensorManager sensorManager = (SensorManager) mActivity
          .getSystemService(Service.SENSOR_SERVICE);
      Sensor tempSensor = sensorManager
          .getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
      if (sensorManager.registerListener(this, tempSensor,
          SensorManager.SENSOR_DELAY_NORMAL))
      {
        MyLog.log(TAG, "register ok");
      }
      else
      {
        MyLog.log(TAG, "register failed");
      }
    }
    catch (Throwable t)
    {
      MyLog.log(TAG, "init: " + t.toString());
    }
  }

  public void deinit()
  {
    try
    {
      mStatus.mState = StatusValue.SHUTDOWN;
      SensorManager sensorManager = (SensorManager) mActivity
          .getSystemService(Service.SENSOR_SERVICE);
      sensorManager.unregisterListener(this);
    }
    catch (Throwable t)
    {
      MyLog.log(TAG, "deinit: " + t.toString());
    }
  }

  public StatusValue getStatus()
  {
    return mStatus;
  }

  public String getStatusRaw (int index)
  {
    return MyTextUtils.sNF1digit.format(mTemp);
  }
  
  public void onAccuracyChanged(Sensor arg0, int arg1)
  {
    MyLog.log(TAG, "accuracyChange: " + arg0.toString() + "=" + arg1);
  }

  @Override
  public void onSensorChanged(SensorEvent arg0)
  {
    try
    {
      float values[] = arg0.values;
      StringBuffer sb = new StringBuffer();
      if (values != null)
      {
        mTemp = values[0];
        for (int i = 0; i < values.length; i++)
        {
          sb.append(values[i]);
          sb.append(";");
        }
        sb.append(values.length);
        mStatus.mString = "Temp=" + values[0];
      }
      String str = sb.toString();
      MyLog.log(TAG, str);
    }
    catch (Throwable t)
    {
      MyLog.log(TAG, "onSensorChanged: " + t.toString());
    }
  }

  public String getName()
  {
    return TAG;
  }
}
