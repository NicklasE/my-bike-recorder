package com.nicklase.test1;

public class StatusValue {
	public final static int UNKNOWN = 0;
	public final static int INITIALIZING = 1;
	public final static int RUNNING = 2;
	public final static int SHUTDOWN = 3;

	public int mState = UNKNOWN;
	public String mString = null;
}
